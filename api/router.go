package api

import (
	"github.com/gin-gonic/gin"
	"hospital/api-gateway/config"
	"hospital/api-gateway/pkg/logger"
	"hospital/api-gateway/services"
	v1 "hospital/api-gateway/api/handlers/v1"

)

type Option struct {
	Conf config.Config
	Logger logger.Logger
	ServiceManager services.IServiceManager
}

func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger: option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg: option.Conf,
	})

	api := router.Group("/v1")

	api.POST("/register/:patient", handlerV1.CreatePatient)

	return router
}

//api.POST("/register/:patients", handlerV1.Register)
