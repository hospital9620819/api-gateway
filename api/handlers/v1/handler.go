package v1

import (
	"hospital/api-gateway/config"
	"hospital/api-gateway/pkg/logger"
	"hospital/api-gateway/services"
)

type handlerV1 struct {
	log            logger.Logger
	serviceManager services.IServiceManager
	cfg            config.Config
}

type HandlerV1Config struct {
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Cfg            config.Config
}

func New(c *HandlerV1Config) *handlerV1 {
	return &handlerV1{
		log: c.Logger,
		serviceManager: c.ServiceManager,
		cfg: c.Cfg,
	}
}
