package v1

import (
	"context"
	"net/http"

	//"ctrconf"
	"time"

	pb "hospital/api-gateway/genproto/patient"
	l "hospital/api-gateway/pkg/logger"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
	//"hospital/api-gateway/pkg/utils"
)
// Summary create patient
// Description This api can patient register
// Tags Patient
// Security BearerAuth
// Accept json
// Produce json
// Param body body models.CreatePatientModel true "CreatePatient"
// Success 201 {object} models.PatientResponse
// Failure 400 string Error response
// Router /v1/create [post]
func (h *handlerV1) CreatePatient(c *gin.Context) {
	var (
		body pb.Patient
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(time.Duration(h.cfg.CtxTimeout)))
	defer cancel()

	response, err := h.serviceManager.PatientService().CreatePatient(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create patient", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}
