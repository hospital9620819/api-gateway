package models

type CreatePatientModel struct {
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Age          int64  `json:"age"`
	Gender       string   `json:"gender"`
	Email        string `json:"email"`
	IllinessName string `json:"illines_name"`
	Diagnosis    string `json:"diagnosis"`
	PhoneNumber  string `json:"phone_number"`
	BuildingId   string `json:"building_id"`
	RoomId       string `json:"room_id"`
	DoctorId     string `json:"doctor_id"`
}

type PatientResponse struct {
	
}
