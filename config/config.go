package config

import (
	"os"
	"github.com/spf13/cast"
)

// Config...
type Config struct {
	Environment string

	PatientServiceHost string
	PatientServicePort int

	CtxTimeout int // context timeout in second

	LogLevel string
	HTTPPort string
}

func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debud"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", "8080"))

	c.PatientServiceHost = cast.ToString(getOrReturnDefault("PATIENT_SERVICE_HOST", "localhost"))
	c.PatientServicePort = cast.ToInt(getOrReturnDefault("PATIENT_SERVICE_PORT", 5000))

	c.CtxTimeout = cast.ToInt(getOrReturnDefault("CTX_TIMEOUT", 7))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}