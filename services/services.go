package services

import (
	"fmt"
	pb "hospital/api-gateway/genproto/patient"
	"hospital/api-gateway/config"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"
)

type IServiceManager interface {
	PatientService() pb.PatientServiceClient
}

type serviceManager struct {
	patientService pb.PatientServiceClient
}

func (s *serviceManager) PatientService() pb.PatientServiceClient {
	return s.patientService
}

func NewServiceManager(conf config.Config) (IServiceManager, error) {
	resolver.SetDefaultScheme("dns")	

	connPatient, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	serviceManager := &serviceManager{
		patientService: pb.NewPatientServiceClient(connPatient),
	}

	return serviceManager, nil
}